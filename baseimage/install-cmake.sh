#!/bin/bash
set -exuo pipefail

tar -zxf ${CMAKE_FILE}.tar.gz
pushd ${CMAKE_FILE}
cmake -DCMAKE_INSTALL_PREFIX=/manylinux-rootfs .
make -j$(nproc)
make install
popd
rm -rf ${CMAKE_FILE} ${CMAKE_FILE}.tar.gz

# Strip what we can
find /manylinux-rootfs -type f -print0 | xargs -0 -n1 strip --strip-unneeded 2>/dev/null || true

# Install
cp -rlf /manylinux-rootfs/* /usr

# Remove temporary rootfs
rm -rf /manylinux-rootfs

hash -r
cmake --version
