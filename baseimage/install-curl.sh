#!/bin/bash
set -exu

MULTIARCH=$(gcc -dumpmachine)

tar -zxf ${CURL_FILE}.tar.gz
pushd ${CURL_FILE}
./configure --with-openssl --prefix=/manylinux-rootfs --libdir=/manylinux-rootfs/lib/$MULTIARCH
make -j$(nproc)
make install
popd
rm -rf ${CURL_FILE} ${CURL_FILE}.tar.gz

# Strip what we can
find /manylinux-rootfs -type f -print0 | xargs -0 -n1 strip --strip-unneeded 2>/dev/null || true

# Install
cp -rlf /manylinux-rootfs/* /usr

# Remove temporary rootfs
rm -rf /manylinux-rootfs

hash -r
curl --version
