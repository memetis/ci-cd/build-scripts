#!/bin/bash
set -exuo pipefail

tar -zxf ${GCC_FILE}.tar.gz
pushd ${GCC_FILE}
git apply /build_scripts/${GCC_FILE}.patch
./contrib/download_prerequisites
popd
mkdir objdir
pushd objdir
MULTIARCH=$(gcc -print-multiarch)
BUILD_OPTIONS="-prefix=/manylinux-rootfs \
    --libdir=/manylinux-rootfs/${MULTIARCH} \
    --enable-multiarch \
    --enable-languages=c,c++,go,fortran \
    --disable-multilib \
    --enable-bootstrap"
if [[ $MULTIARCH == *"arm"* ]]; then
  BUILD_OPTIONS="${BUILD_OPTIONS} \
    --build=${MULTIARCH}"
fi
$PWD/../${GCC_FILE}/configure ${BUILD_OPTIONS}
    
make -j$(nproc)
make install
popd
rm -rf ${GCC_FILE} ${GCC_FILE}.tar.gz objdir

# Strip what we can
find /manylinux-rootfs -type f -print0 | xargs -0 -n1 strip --strip-unneeded 2>/dev/null || true

# Install
cp -rlf /manylinux-rootfs/* /usr

# Remove temporary rootfs
rm -rf /manylinux-rootfs

hash -r
gcc --version
