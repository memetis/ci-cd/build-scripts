#!/bin/bash
set -exu

tar -zxf ${BINUTILS_FILE}.tar.gz
mkdir binutils-build
pushd binutils-build
if ../${BINUTILS_FILE}/configure --prefix=/manylinux-rootfs --disable-nls ; then
  make -j$(nproc) all
  make install
else
  cat config.log
  exit 1
fi
popd
rm -rf ${BINUTILS_FILE} ${BINUTILS_FILE}.tar.gz binutils-build

# Strip what we can
find /manylinux-rootfs -type f -print0 | xargs -0 -n1 strip --strip-unneeded 2>/dev/null || true

# Install
cp -rlf /manylinux-rootfs/* /usr

# Remove temporary rootfs
rm -rf /manylinux-rootfs

hash -r
ld --version
