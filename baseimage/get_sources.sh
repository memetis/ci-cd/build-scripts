#!/bin/bash
set -exuo pipefail

# esnsure source directory exists
mkdir -p baseimage/cache/
pushd baseimage/cache/

get_file() {
    # get source file
    if ! test -f "$1.tar.gz"; then
        url=$(eval echo $3)
        curl -fsSL -o "$1.tar.gz" "${url}/$1.tar.gz"
    else
        echo "$1 exists, skipping download"
    fi

    # validate checksum
    echo "$2  $1.tar.gz" > $1.sha256
    if sha256sum -c $1.sha256 ; then
        echo "$1 checksum matches"
    else
        rm -f $1.tar.gz
        exit 1
    fi
    rm -f $1.sha256
}

get_file "${BINUTILS_FILE}" "${BINUTILS_HASH}" 'https://ftp.gnu.org/gnu/binutils'
get_file "${CMAKE_FILE}" "${CMAKE_HASH}" "https://github.com/Kitware/CMake/releases/download/v$(echo $CMAKE_FILE | cut -d "-" -f 2)"
get_file "${CURL_FILE}" "${CURL_HASH}" 'https://curl.se/download'
get_file "${GCC_FILE}" "${GCC_HASH}" "https://ftp.fu-berlin.de/unix/languages/gcc/releases/${GCC_FILE}"

popd
