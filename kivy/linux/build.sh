#!/bin/bash

# Echo commands and exit on error
set -xe

# Remove old
if test -d "build"; then
  rm -rf "build"
fi
if test -d "dist"; then
  rm -rf "dist"
fi

# Get metadata
app_version=$(grep "version" "pyproject.toml" | cut -d'"' -f 2)
echo "App Version to build: $app_version"

commit_full_hash=$(git rev-parse HEAD)
commit_hash=${commit_full_hash:0:16}
echo "Git commit to build: $commit_hash"

kivy_version=$(grep -Eo 'kivy == ([0-9.]*)' pyproject.toml | cut -d' ' -f 3)
echo "Kivy Version to be used: $kivy_version"

kivymd_version=$(grep -Eo 'kivymd [^"]*' pyproject.toml | grep -Eo 'archive/[^.]*' | cut -d'/' -f 2)
echo "KivyMD Version to be used: $kivymd_version"

app_python_package=$(grep -Eo '^name.*' pyproject.toml | cut -d'"' -f 2)
echo "App Python Package name to build: $app_python_package"

app_name=$(grep "APP_NAME" "$app_python_package/setting/defaults.py" | cut -d"'" -f 2)
echo "App Name to build: $app_name"

# Change to build directory
mkdir -p "build"
mkdir -p "dist"
cd "build"
echo "Building in $(pwd)"

# Get appimagetool and kivy as appimage
curl -L -o appimagetool "https://gitlab.com/api/v4/projects/memetis%2Fci-cd%2Fbuild-scripts/packages/generic/appimagetool/${POLICY}/appimagetool_${PLATFORM}.AppImage"
chmod +x appimagetool
curl -L -o kivy_appimage "https://gitlab.com/api/v4/projects/memetis%2Fci-cd%2Fbuild-scripts/packages/generic/kivy/${POLICY}/kivy_${PYTHON_VERSION}_${kivy_version}_${PLATFORM}.AppImage"
chmod +x kivy_appimage

# Extract Python appimage
APPIMAGE_EXTRACT_AND_RUN=1 ./kivy_appimage --appimage-extract
mv squashfs-root app

# Export library paths
APPDIR="$(pwd)/app"
export LD_LIBRARY_PATH="${APPDIR}/usr/lib/:$LD_LIBRARY_PATH"
export TCL_LIBRARY="${APPDIR}/usr/share/tcltk/tcl8.5"
export TK_LIBRARY="${APPDIR}/usr/share/tcltk/tk8.5"
export TKPATH="${TK_LIBRARY}"

# Ignore system Python
export PYTHONNOUSERSITE=1

# Install exact version of KivyMD
./app/AppRun -m pip install --upgrade "https://github.com/kivymd/KivyMD/archive/${kivymd_version}.zip"

# Install GUI application
./app/AppRun -m pip install --upgrade ../

# Install AppRun
cp ../build-scripts/kivy/linux/AppRun app
chmod +x app/AppRun
sed -i "s/REPLACEME/$app_python_package/g" app/AppRun

# Create AppImage
app_image_name="${app_name}_Raspberry Pi_${PLATFORM}_${app_version}_${commit_hash}.AppImage"
APPIMAGE_EXTRACT_AND_RUN=1 ./appimagetool app "$app_image_name"
mv "$app_image_name" "../dist/"
