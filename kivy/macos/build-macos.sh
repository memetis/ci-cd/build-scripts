#!/bin/sh

# Echo commands and exit on error
set -x
set -e

# Remove old
if test -d "build"; then
  rm -rf "build"
fi
if test -d "dist"; then
  rm -rf "dist"
fi

# Get metadata
app_version=$(grep "version" "pyproject.toml" | cut -d'"' -f 2)
echo "App Version to build: $app_version"

commit_full_hash=$(git rev-parse HEAD)
commit_hash=${commit_full_hash:0:16}
echo "Git commit to build: $commit_hash"

kivy_version=$(grep -Eo 'kivy == ([0-9.]*)' pyproject.toml | cut -d' ' -f 3)
echo "Kivy Version to be used: $kivy_version"

kivymd_version=$(grep -Eo 'kivymd [^"]*' pyproject.toml | grep -Eo 'archive/[^.]*' | cut -d'/' -f 2)
echo "KivyMD Version to be used: $kivymd_version"

app_python_package=$(grep -Eo '^name.*' pyproject.toml | cut -d'"' -f 2)
echo "App Python Package name to build: $app_python_package"

app_name=$(grep "APP_NAME" "$app_python_package/setting/defaults.py" | cut -d"'" -f 2)
echo "App Name to build: $app_name"

app_author='memetis GmbH'
app_identifier="org.memetis.${app_python_package}"
app_icon='app_icon.png'

# Change to build directory
mkdir -p "build"
mkdir -p "dist"
mkdir -p "cache"
cd "build"
echo "Building in $(pwd)"

# Get kivy-sdk-packager
git clone -q --depth 1 "https://github.com/kivy/kivy-sdk-packager.git"

# Get pre-built Kivy image, as a result a basic Kivy.app is available in build/$app_name.app
kivy_dmg="Kivy-$kivy_version.dmg"
kivy_url="https://kivy.org/downloads/$kivy_version/Kivy.dmg"
if ! test -f "../cache/$kivy_dmg"; then
  curl -o "../cache/$kivy_dmg" -L "$kivy_url"
fi
hdiutil attach "../cache/$kivy_dmg" -mountroot .
cp -R Kivy/Kivy.app "$app_name.app"
sync
sleep 10
hdiutil unmount Kivy -force

# Fix venv activate file
echo "Fix venv activate file"
sed -i '' 's/VIRTUAL_ENV=.*/VIRTUAL_ENV=$(cd "$(dirname "$BASH_SOURCE")"; dirname "`pwd`")/g' "$app_name.app/Contents/Resources/venv/bin/activate"

# Use custom script file to launch Python application when app is started
echo "Copy script file"
cp "../build-scripts/kivy/macos/script" "$app_name.app/Contents/Resources/script"
sed -i '' "s/REPLACE_MY_BY_PYTHON_PACKAGE/$app_python_package/g" "$app_name.app/Contents/Resources/script"

# Set metadata
./kivy-sdk-packager/osx/fix-bundle-metadata.sh "$app_name.app" -n "$app_name" -v "$app_version" -a "$app_author" -o "$app_identifier" -i "../$app_icon"

# Activate Python environment
pushd "$app_name.app/Contents/Resources/venv/bin"
source activate
popd

# Update pip
python -m pip install -U pip wheel

# Binary wheels need to be converted to universal2 wheels so they run on both Intel and arm64 Macs
echo "Check for installation of universal2 wheels"
python -m pip install delocate
python_install_directory=$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")
if [ ! -d "universal2-wheel" ]; then
    mkdir "universal2-wheel"
fi
create_universal2_wheel () {
    python -m pip download --only-binary=:all: --platform macosx_10_9_x86_64 "$1"
    python -m pip download --only-binary=:all: --platform macosx_11_0_arm64 "$1"
    short_name=$(echo $1 | cut -d'-' -f 1)
    delocate-fuse $short_name* -w "universal2-wheel"
    python -m pip install --target "$python_install_directory" --only-binary=:all: --platform macosx_10_9_x86_64 --platform macosx_11_0_arm64 --upgrade --ignore-installed universal2-wheel/$short_name*
}
use_universal2_10_9_wheel () {
    python -m pip install --target "$python_install_directory" --only-binary=:all: --platform macosx_10_9_universal2 --upgrade --ignore-installed --no-deps $1
}
use_universal2_10_12_wheel () {
    python -m pip install --target "$python_install_directory" --only-binary=:all: --platform macosx_10_12_universal2 --upgrade --ignore-installed --no-deps $1
}
if [ ! -z "$PYTHON_USE_BINARY_PANDAS" ]; then
    echo "Installing pandas as universal2 with it's binary dependencies"
    create_universal2_wheel kiwisolver
    python -m pip install cycler
    create_universal2_wheel contourpy
    use_universal2_10_9_wheel fonttools
    use_universal2_10_12_wheel matplotlib
    create_universal2_wheel pandas
    create_universal2_wheel numpy
fi
if [ ! -z "$PYTHON_USE_BINARY_PSYCOPG2" ]; then
    echo "Installing psycopg2-binary as universal2"
    create_universal2_wheel psycopg2-binary
fi

# Install exact version of KivyMD
python -m pip install "https://github.com/kivymd/KivyMD/archive/${kivymd_version}.zip"

# Install GUI application
python -m pip install --upgrade ../

# Deactive Python environment
deactivate

# Reduce file size
./kivy-sdk-packager/osx/cleanup-app.sh "$app_name.app" -g 1

# Build DMG
./kivy-sdk-packager/osx/relocate.sh "$app_name.app"
./kivy-sdk-packager/osx/create-osx-dmg.sh "$app_name.app" "$app_name" -b "../build-scripts/kivy/macos/dmg_background.png"

# Move to dist directory
mv "$app_name.dmg" "../dist/${app_name}_${app_version}_${commit_hash}.dmg"

# Clean-up
sync
sleep 10