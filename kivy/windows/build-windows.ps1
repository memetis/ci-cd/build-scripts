$defaults_file = Get-ChildItem -Filter 'defaults.py' -Recurse -Name | Select-Object -First 1
foreach($line in Get-Content $defaults_file) {
	if($line -match 'APP_NAME.*'){
		$version_substring = ($line -split "'*..=")[1]
		$app_name = $version_substring.substring(2, $version_substring.IndexOf("'", 2) - 2)
	}
}
Write-Host "App Name to build: $app_name"

$pyproject_file = Get-ChildItem -Filter 'pyproject.toml' -Recurse -Name | Select-Object -First 1
foreach($line in Get-Content $pyproject_file) {
	if($line -match 'version.*'){
		$version_substring = ($line -split "'*..=")[1]
		$app_version = $version_substring.substring(2, $version_substring.IndexOf('"', 2) - 2)
	}
}
Write-Host "App Version to build: $app_version"

$commit_hash = (git rev-parse HEAD).Substring(0,16)
Write-Host "Git commit to build: $commit_hash"

foreach($line in Get-Content $pyproject_file) {
	if($line -match 'kivy .*'){
		$version_substring = ($line -split "'*..=")[1]
		$kivy_version = $version_substring.substring(2, $version_substring.IndexOf('"', 2) - 2)
	}
}
Write-Host "Kivy Version to be used: $kivy_version"

python -V
python -m pip install -U pip wheel virtualenv

Remove-Item build_venv -Recurse
python -m virtualenv build_venv
Push-Location build_venv
.\Scripts\activate.ps1
Pop-Location


python -m pip install "kivy==$kivy_version"
& "$PSScriptRoot\patch-pyinstaller-kivy-hook.ps1"
python -m pip install -U PyInstaller pyinstaller-versionfile
python -m pip install .
New-Item -ItemType Directory -Force -Path build
python build-metadata.py
create-version-file build/metadata.yml --outfile build/metadata.bin
python -m PyInstaller --log-level INFO --clean pyinstaller.spec

$exe_file = Get-ChildItem -path dist/ -filter *.exe -file
Rename-Item -Path dist/$exe_file -NewName ($app_name + "_" + $app_version + "_" + $commit_hash + ".exe")